#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Code to reproduce Figure 7 and related analyses of Fabus et al (2022).
Created on Fri Dec 17 11:51:43 2021

@author: MSFabus
"""
import emd
import  numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from scipy.signal import iirnotch, filtfilt, decimate, welch
import dcor 

plt.rcParams['figure.dpi'] = 200
save = False

# Load data
f = '/media/marco/MSFabus_DPhil2/Datasets/itEMD/hc-3/ec013.30/ec013.454/ec013.454.eeg'
chan = 65  # Found in xml file

raw = np.fromfile(f, dtype=np.int16).astype(float)
raw = raw.reshape(-1,chan)[:,10]

data = decimate(raw[:60*1250] / 1000, 2)
sample_rate = int(1250 / 2)
t = np.linspace(0, 5, sample_rate*5)

# Prepare output
wfs = np.zeros((8, 48))
pmsi = np.zeros(4)
N = np.zeros(4)

freqs, psd = welch(data, sample_rate, nperseg=4*sample_rate, scaling='spectrum')

# Apply masked EMD
imf = emd.sift.mask_sift(data, mask_freqs='zc', max_imfs=8, mask_amp_mode='ratio_sig',
                         sift_thresh=1e-8)    

#%% Plotting
plt.rc('font', size=10)
fig, (ax1, ax2, ax3) = plt.subplots(1, 3, 
                                   gridspec_kw={'width_ratios': [1., 3, 1.]},
                                   figsize=(7.0625, 2.))
ax1.annotate('A', xy=(-18, 0.35), fontsize=24, annotation_clip=False)
ax2.annotate('B', xy=(-0.1, 1.063), fontsize=24, annotation_clip=False)
ax3.annotate('C', xy=(-2, 16.), fontsize=24, annotation_clip=False)

ax1.semilogy(freqs, psd, color='k', lw=1)
ax1.set_xlim([0, 30])
ax1.set_ylim([5e-5, 0.2])
ax1.set_ylabel('Spectral Density')
ax1.set_xlabel('Freq [Hz]')
ax1.grid()
ax2.axis('off')

secs = [16, 17.5]
idx = list(range(int(secs[0]*sample_rate), int(secs[1]*sample_rate)))
imfs = imf[idx, :]
nplots = 5
time_vect = np.linspace(0, imfs.shape[0]/sample_rate, imfs.shape[0])

mx = np.abs(imfs).max()
mx_sig = np.abs(imfs.sum(axis=1)).max()

plt.rc('font', size=7)
left, bottom, width, height = [0.35, 0.75, 0.34, 0.15]
ax = fig.add_axes([left, bottom, width, height])

ax.yaxis.get_major_locator().set_params(integer=True)
for tag in ['top', 'right', 'bottom']:
    ax.spines[tag].set_visible(False)
ax.plot((time_vect[0], time_vect[-1]), (0, 0), color=[.5, .5, .5], lw=1)
ax.plot(time_vect, imfs.sum(axis=1), 'k', lw=1)
ax.tick_params(axis='x', labelbottom=False)
ax.set_xlim(time_vect[0], time_vect[-1])
ax.set_ylim(-mx_sig * 1.1, mx_sig * 1.1)
ax.set_ylabel('Signal', rotation=90, labelpad=5)

cmap = plt.cm.Dark2
cols = cmap(np.linspace(0, 1, imfs.shape[1] + 1))

for ii in range(1, nplots):
    left, bottom, width, height = [0.35, 0.75-0.15*ii, 0.34, 0.115]
    ax = fig.add_axes([left, bottom, width, height])
    
    for tag in ['top', 'right', 'bottom']:
        ax.spines[tag].set_visible(False)
    ax.plot((time_vect[0], time_vect[-1]), (0, 0), color=[.5, .5, .5], lw=1)
    ax.plot(time_vect, imfs[:, ii - 1], color=cols[ii, :], lw=1)
    ax.set_xlim(time_vect[0], time_vect[-1])
    ax.set_ylim(-mx * 1.4, mx * 1.4)
    ax.yaxis.get_major_locator().set_params(integer=True)
    ax.set_ylabel('IMF{0}'.format(ii), rotation=90, labelpad=5)

    if ii < nplots - 1:
        ax.tick_params(axis='x', labelbottom=False)
        ax.set_xticks([0, 0.25, 0.5, 0.75, 1, 1.25, 1.5])
    else:
        pass
        ax.tick_params(axis='x', labelbottom=False)
        ax.set_xticks([0, 0.25, 0.5, 0.75, 1, 1.25, 1.5])
    
# Show added IMFs too
left, bottom, width, height = [0.35, 0.75-0.15*(ii+1), 0.34, 0.12]
ax = fig.add_axes([left, bottom, width, height])
for tag in ['top', 'right', 'bottom']:
    ax.spines[tag].set_visible(False)
ax.plot((time_vect[0], time_vect[-1]), (0, 0), color=[.5, .5, .5], lw=1)
ax.plot(time_vect, imfs[:, 2] + imfs[:, 3], color='purple', lw=1)
ax.set_xlim(time_vect[0], time_vect[-1])
ax.set_ylim(-mx * 1.5, mx * 1.5)
ax.yaxis.get_major_locator().set_params(integer=True)
ax.set_xticks([0, 0.25, 0.5, 0.75, 1, 1.25, 1.5])
ax.set_ylabel('IMF\n(3+4)', rotation=90, labelpad=2)
ax.set_xlabel('Time [s]')

# Find cycles for base, harmonic, added
ix = [1, 2, 3, 4]
labs = ['IMF-2', 'IMF-3', 'IMF-4', 'IMF-(3+4)']
cols2 = ['', cols[3, :], cols[4, :], 'purple']

plt.rc('font', size=10)

for i in range(1,4):
    
    imf_r = imf[:, ix[i]]
    if i == 3:
        imf_r = imf[:, 3] + imf[:, 2] 
    IP, IF, IA = emd.spectra.frequency_transform(imf_r, sample_rate, 'hilbert')
    amp_thresh = np.percentile(IA[:, 0], 50)
    mask = IA[:, 0] > amp_thresh
    cycles = emd.cycles.get_cycle_vector(IP[:, 0], return_good=True)
    N = cycles.max()
    
    cycles2 = emd.cycles.get_cycle_vector(IP[:, 0], return_good=False)#, mask=mask)
    print('%.1f / %.1f cycles good' %(N, cycles2.max()))
    
    pa, ph = emd.cycles.phase_align(IP[:, 0], IF[:, 0], cycles=cycles)
    if_m = pa.mean(1)
    er = np.std(pa, 1) / (np.sqrt(N))
    ax3.plot(ph, if_m, label=labs[i], linewidth=1, color=cols2[i], zorder=i)
    ax3.fill_between(ph, if_m-er, if_m+er, alpha=0.5, color=cols2[i], zorder=i)
            
ax3.set_xlabel('Phase [rad]')
ax3.set_ylabel('IF [Hz]', labelpad=0)
plt.rc('font', size=6)
leg = ax3.legend(ncol=1, loc='center')
for line in leg.get_lines():
    line.set_linewidth(2.0)
ax3.set_xticks([0, np.pi, 2*np.pi])
plt.rc('text', usetex=True)
ax3.set_xticklabels([0, '$\pi$', '$2 \pi$'])
plt.rc('text', usetex=False)
ax3.set_xlim(0, 2*np.pi)
ax3.tick_params(axis='x')
ax3.grid(True)
ax3.set_axisbelow(True)
plt.show()

if save:
    fname = 'fig7.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)


#%% Test which IMFs are harmonics
# Split into 20 segments and test harmonic conditions
Nsplit = 20
IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'hilbert')
IFs = np.array(np.split(IF, Nsplit))
IPs = np.array(np.split(IP, Nsplit))
IAs = np.array(np.split(IA, Nsplit))

IFms = np.mean(IFs, 1)
IAms = np.mean(IAs, 1)

m_base = 3
Ntest = 3 # base w/ IMF-0, IMF-1, IMF-2
fratios = np.zeros((Ntest, Nsplit))
a_s = np.zeros((Ntest, Nsplit))
afs = np.zeros((Ntest, Nsplit))
dcorrs = np.zeros((Ntest, Nsplit))
dcor_pvals = np.zeros((Ntest, 2))

for i in range(Ntest):
    # Freq ratios
    f = IFms[:, i] / IFms[:, m_base]
    fratios[i] = f
    # af
    a = IAms[:, i] / IAms[:, m_base]
    af = a * f
    a_s[i] = a
    afs[i] = af
    # Test 1: significant PPC 
    dcorr = dcor.distance_correlation(IP[:, i], IP[:, m_base])
    p_dcor, _ = dcor.independence.distance_correlation_t_test(IP[:, i], IP[:, m_base])
    dcor_pvals[i, :] = dcorr, p_dcor
    for j in range(Nsplit):
        dcorr = dcor.distance_correlation(IPs[j, :, i], IPs[j, :, m_base])
        dcorrs[i, j] = dcorr

ints = np.round(np.mean(fratios, 1))
fratio_pvals = np.zeros(Ntest)
af_pvals = np.zeros(Ntest)
for i in range(Ntest):
    # Test 2: frequency ratio not different from nearest integer
    tstat, p = stats.ttest_1samp(fratios[i, :], ints[i])
    fratio_pvals[i] = p
    # Test 3: af < 1
    tstat, p = stats.ttest_1samp(afs[i, :], 1, alternative='less')
    af_pvals[i] = p

#%% Find average properties and print them - TABLE 1 in Manuscript
import statsmodels.api as sm
fig, axs = plt.subplots(2, 3, figsize=(12, 6))
for i in range(4):
    print('Mode = %s'%i)
    print('IF = %.2f +/- %.3f'%(np.mean(IFms[:, i], 0), np.std(IFms[:, i])))
    print('IA = %.2f +/- %.3f'%(np.mean(IAms[:, i], 0), np.std(IAms[:, i])))
    if i < 3:
        print('dcor = %.3f' % dcor_pvals[i, 0])
        print('Freq ratio = %.1f +/- %.2f' % (np.mean(fratios[i, :]), np.std(fratios[i, :])))
        print('Amp ratio = %.2f +/- %.2f' % (np.mean(a_s[i, :]), np.std(a_s[i, :])))
        ax = axs.flatten()[i]
        ax2 = axs.flatten()[i+3]
        sm.graphics.tsa.plot_acf(fratios[i, :], lags=19, alpha=0.01, title='f, IMF-%d'%(i+1), ax=ax)
        sm.graphics.tsa.plot_acf(a_s[i, :], lags=19, alpha=0.01, title='a, IMF-%d'%(i+1), ax=ax2)
    if i == 3:
        print('dcor = 1')
        print('Freq ratio = 1')
        print('Amp ratio = 1')
    print('\n')
fig.text(0.5, 0.04, 'Lag', ha='center', fontsize=20)
fig.text(0.07, 0.5, 'Autocorrelation', va='center', rotation='vertical', fontsize=20)

plt.subplots_adjust(hspace=0.5)
plt.show()

#%% For Reviewers - Test autocorrelation with Durbin-Watson
from statsmodels.stats.stattools import durbin_watson
print(durbin_watson(a_s[2, :] - np.mean(a_s[2, :])))