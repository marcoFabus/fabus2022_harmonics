#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Code to reproduce Figure 6 and related analyses of Fabus et al (2022).
Created on Fri Dec 17 11:51:43 2021

@author: MSFabus
"""
import numpy as np
from matplotlib import pyplot as plt
from neurons import FNNeuron
from scipy import signal, stats
from scipy.fft import fft, fftfreq
import emd

plt.rcParams['figure.dpi'] = 200
save = False

# Parameters
I_ampl = 0.475
tmax = 10000.
V_0 = -0.
W_0 = -0.4
Fs = 100
N = int(Fs*tmax)

# Create neuron and calculate harmonic properties
ts, t_step = np.linspace(0, tmax, N, retstep=True)
neuron = FNNeuron(I_ampl=I_ampl, V_0=V_0, W_0=W_0)
neuron.solve(ts=ts)
Vs = neuron.Vs

freqs, psd = signal.welch(Vs, fs=100*1000, nperseg=100000)

Fs = 100*1000
yf = fft(Vs, )
xf = fftfreq(N, 1/Fs)[:N//2]
yf_real = 2.0/N * np.abs(yf[0:N//2])

peaks, props = signal.find_peaks(yf_real, distance=1, height=0.0015)
y_peaks = np.array([yf_real[x] for x in peaks[:30]])
f_peaks = np.array([xf[x] for x in peaks[:30]])

# Fit 1/x^gamma model
slope, intercept, r, p, se = stats.linregress(np.log10(f_peaks), np.log10(y_peaks))

freq = np.linspace(20, 1270, 100)
ypred_oof = 10**(intercept + slope*np.log10(freq))

# Find IF
IP, IF, IA = emd.spectra.frequency_transform(Vs, Fs, 'hilbert')

 
#%% Plotting
fig, axs = plt.subplots(2, 3, figsize=(7.0625, 4.5), 
                              gridspec_kw={'width_ratios': [2., 2, 2.]},)

ax = axs.flatten()[2]
ax2 = axs.flatten()[0]
ax3 = axs.flatten()[1]

ax4 = axs.flatten()[3]
ax5 = axs.flatten()[4]
ax6 = axs.flatten()[5]

ax2.annotate('A', xy=(-70, 2.25), fontsize=20, annotation_clip=False)
ax2.annotate('B', xy=(220, 2.25), fontsize=20, annotation_clip=False)
ax2.annotate('C', xy=(550, 2.25), fontsize=20, annotation_clip=False)

ax2.annotate('D', xy=(-70, -7), fontsize=20, annotation_clip=False)
ax2.annotate('E', xy=(220, -7), fontsize=20, annotation_clip=False)
ax2.annotate('F', xy=(550, -7), fontsize=20, annotation_clip=False)

ax.loglog(xf, yf_real , c='k', lw=1)
ax.scatter(f_peaks, y_peaks, s=15, color='red', label='Harmonic\npeaks')
ax.loglog(freq, ypred_oof, c='b', label='$1/f^{\gamma}$ fit')

ax.set_xlim(20, 860)
ax.set_ylim(5e-4, 3)
ax.set_xticks([25, 100, 400,])
ax.set_xticklabels(['25', '100', '400',])
ax.legend(fontsize=8, ncol=2, bbox_to_anchor=(1.2, 1.5), loc='upper right')
ax.minorticks_off()
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('PSD', labelpad=2)

ax2.plot(ts, neuron.Vs, lw=1, c='k')
ax2.set_xlim(0, 200)
ax2.set_ylabel('Signal', labelpad=3)
ax2.set_xlabel('Time [ms]')


ax3.plot(ts[2:], IF[2:, 0], lw=1, c='k')
ax3.set_xlim(0, 200)
ax3.set_ylim(0, 100)
ax3.set_xlabel('Time [ms]')
ax3.set_ylabel('IF [Hz]', labelpad=3)

plt.subplots_adjust(wspace=0.6, hspace=0.45)


#%% Same as above but with Abreu 2010

Fs = 10000
tmax = 100
N = int(Fs*tmax)
ts, t_step = np.linspace(0, tmax, N, retstep=True)
freq = 1

# Change extent of deformation from sinusoidal shape [-1 to 1]
nonlinearity_deg = 0.75
# Change left-right skew of deformation [-pi to pi]
nonlinearity_phi = -np.pi/4

# Create a non-linear oscillation
x = emd.utils.abreu2010(freq, nonlinearity_deg, nonlinearity_phi, Fs, tmax)

freqs, psd = signal.welch(x, fs=Fs, nperseg=Fs)

yf = fft(x, )
xf = fftfreq(N, 1/Fs)[:N//2]
yf_real = 2.0/N * np.abs(yf[0:N//2])

# <10Hz
peaks, props = signal.find_peaks(yf_real, distance=1, height=1e-5)
y_peaks = np.array([yf_real[x] for x in peaks[:9]])
f_peaks = np.array([xf[x] for x in peaks[:9]])
# Fit 1/x^gamma model
slope, intercept, r, p, se = stats.linregress(np.log10(f_peaks), np.log10(y_peaks))
freq = np.linspace(0.5, 20, 100)
ypred_oof = 10**(intercept + slope*np.log10(freq))

# >10Hz
peaks, props = signal.find_peaks(yf_real, distance=1, height=2e-6)
y_peaks2 = np.array([yf_real[x] for x in peaks[9:]])
f_peaks2 = np.array([xf[x] for x in peaks[9:]])
# Fit 1/x^gamma model
slope2, intercept2, r2, p2, se = stats.linregress(np.log10(f_peaks2), np.log10(y_peaks2))
freq2 = np.linspace(6, 270, 100)
ypred_oof2 = 10**(intercept2 + slope2*np.log10(freq2))


# Find IF
IP, IF, IA = emd.spectra.frequency_transform(x, Fs, 'hilbert')

#%% Plotting

ax = ax6
ax2 = ax4
ax3 = ax5

ax.loglog(xf, yf_real , c='k', lw=1)
ax.scatter(f_peaks, y_peaks, s=25, color='red', label='Harmonic\npeaks <10Hz')
ax.loglog(freq, ypred_oof, c='blue', label='$1/f^{\gamma}$ fit \n<10Hz')
ax.scatter(f_peaks2, y_peaks2, s=25, color='green', label='Harmonic\npeaks $\geq$10Hz')
ax.loglog(freq2, ypred_oof2, c='deepskyblue', label='$1/f^{\gamma}$ fit \n $\geq$10Hz')

ax.set_xlim(0.5, 30)
ax.set_ylim(1e-6, 2)
ax.set_xticks([1, 10])
ax.set_xticklabels(['1', '10',])
ax.legend(fontsize=8, ncol=4, bbox_to_anchor=(1.2, 1.45), loc='upper right')
ax.minorticks_off()
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('PSD', labelpad=2)

ax2.plot(ts, x, lw=1, c='k')
ax2.set_xlim(0, 3)
ax2.set_ylabel('Signal', labelpad=3)
ax2.set_xlabel('Time [s]')
ax2.set_ylim([-1, 1.4])


ax3.plot(ts[2:], IF[2:, 0], lw=1, c='k')
ax3.set_xlim(0, 3)
ax3.set_ylim(0, 2)
ax3.set_yticks([0, 1, 2])
ax3.set_xlabel('Time [s]')
ax3.set_ylabel('IF [Hz]', labelpad=3)

plt.subplots_adjust(wspace=0.6, hspace=0.95)

if save:
    fname = 'fig_Abreu.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()

#%% For Reviewers - Test decomposition with noise
x2 = x + np.random.normal(0, 0.1, len(x))
imf = emd.sift.mask_sift(x2[:10*Fs], mask_freqs='zc', max_imfs=16, mask_amp_mode='ratio_sig',
                         sift_thresh=1e-8)  
# #%%
# emd.plotting.plot_imfs(imf[:10*Fs, 8:12], cmap=True, scale_y=True)
# IP, IF, IA = emd.spectra.frequency_transform(imf[:10*Fs, 8:12], Fs, 'hilbert')