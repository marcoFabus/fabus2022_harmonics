# Fabus2022_Harmonics

This repository containts scripts to reproduce figures from [Fabus et al. (2022)](https://www.biorxiv.org/content/10.1101/2021.12.21.473676v1). 

Additionally, we include a version of our [shape generator website](shapegen.herokuapp.com/) to be used locally (run the app.py code and go to [127.0.0.1:8050/](127.0.0.1:8050/) in your browser), as well as a Jupyter notebok guiding the reader through some of the main points of the manuscript.

Finally, the theory outlined in this paper has now been implemented into the [EMD-Python toolbox](https://emd.readthedocs.io/en/stable/), specifically the functions [emd.imftools.assess_harmonic_criteria](https://emd.readthedocs.io/en/stable/stubs/emd.imftools.assess_harmonic_criteria.html), [emd.imftools.assess_joint_if](https://emd.readthedocs.io/en/stable/stubs/emd.imftools.assess_joint_if.html), and an [associated tutorial](https://emd.readthedocs.io/en/stable/emd_tutorials/02_spectrum_analysis/emd_tutorial_02_spectrum_04_harmonic_structures.html#emd-implementation).

FitzHugh-Nagumo implementation was adapted from [brainythings](https://github.com/ruhugu/brainythings).

Corresponding author: Marco S Fabus, marco.fabus@ndcn.ox.ac.uk.
